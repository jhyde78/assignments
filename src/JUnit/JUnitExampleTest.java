
// Jarom Hyde


package JUnit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JUnitExampleTest {

    @Test
    public void mathTest() {
        JUnitExample test = new JUnitExample();
        int result = test.math(4, 2, 4, 2);

        // Throws a redundant caution due to int never being null
        assertNotNull(result);

        assertTrue (5 <= result);

        assertFalse(5 >= result);

        // Uncomment line 26 and the code will break
        // due to the result not being null.
        // assertNull(result);

        assertSame(10, result);

        assertNotSame(5, result);

        assertEquals(10, result);

    }
}
