
// Jarom Hyde

import java.util.HashSet;

public class CollectionsHashSet {

    public static void  main(String[] args) {

        HashSet<String> set = new HashSet<String>();
            set.add("Primaris Space Marines");
            set.add("Orks");
            set.add("Daemons of Slaanesh");
            set.add("Necron Dyasty");

            System.out.println(set);
    }
}
