
// Jarom Hyde

import java.util.ArrayList;
import java.util.Iterator;

public class CollectionsQueue {

    public static void main(String[] args) {

        ArrayList<String> armies = new ArrayList<String>();
        armies.add("Primaris Space Marines");
        armies.add("Adeptus Sororitas");
        armies.add("Daemons of Slaanesh");
        armies.add("Ork Bois");
        armies.add("Plague Marines");
        armies.add("Noisy Bois");

        Iterator iterator = armies.iterator();

        while (iterator.hasNext()) {
            String obj = (String) iterator.next();
            System.out.println(obj);
        }
    }
}

