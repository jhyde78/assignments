
// Jarom Hyde

import java.util.HashMap;

public class CollectionHashMap {

    public static void main(String[] args) {
        HashMap<String, Integer> points = new HashMap<String, Integer>();
        points.put("Space Marines", 50);
        points.put("Necron", 48);
        points.put("Slaanesh", 44);
        points.put("Orks", 49);

        System.out.println(points);
    }
}
