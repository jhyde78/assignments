
// Jarom Hyde

package FinalApp;

import java.util.*;
import java.lang.*;
import java.util.HashMap;

public class CollectionApp {

    public static void Collect() {

        System.out.print("\nThe following is a printout of an array list of various units\n" +
                "for the game Warhammer 40k:\n\n");

        {
            HashMap<String, Integer> points = new HashMap<>();
            points.put("\nSpace Marines", 50);
            points.put("\nNecron", 48);
            points.put("\nSlaanesh", 44);
            points.put("\nOrks", 49);
            points.put("\nDrakhari", 47);
            points.put("\nChaos Marines", 46);
            points.put("\nTau", 43);
            points.put("\nTyranids", 47);
            points.put("\nAdeptas Sororitas", 50);

            System.out.println(points);

            System.out.println("Thank you for trying my Collection App.\n");
            System.out.println("Now where would you like to go?\n");
            System.out.println("Please select from the following options: ");
            System.out.println("Enter 1 for Exception Example");
            System.out.println("Enter 2 for Thread Example");
            System.out.println("Enter 3 for JUnit Example");
            System.out.println("Enter 4 to quit");
            System.out.println("Please enter a number from 1 to 4.");

            Scanner scanIn = new Scanner(System.in);
            int scanEntry = 0;
            boolean valid = false;
            while (!valid) {
                try {
                    scanEntry = scanIn.nextInt();
                    if (scanEntry >= 1 && scanEntry <= 4) {
                        valid = true;
                    } else {
                        System.out.println("The number you entered is outside the range.");
                    }
                } catch (InputMismatchException e) {
                    System.out.println("That was not a valid entry.");
                    scanIn.next();
                }
            }

            switch (scanEntry) {
                case 1 -> {
                    ExceptionApp Except = new ExceptionApp();
                    Except.Except();
                }
                case 2 -> {
                    ThreadApp run = new ThreadApp();
                    run.run();
                    ThreadApp Thread = new ThreadApp();
                    Thread.ThreadRun();
                }
                case 3 -> {
                    JunitAPP JUnit = new JunitAPP();
                    JUnit.JUnit();
                }
                case 4 -> {
                    System.out.println("Thank you for using this simple app.");
                    System.exit(0);
                }
            }
        }
    }
}