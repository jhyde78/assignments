
// Jarom Hyde

package FinalApp;

import java.util.InputMismatchException;
import java.util.Scanner;


public class FinalAppMain {

    public static void main(String[] args) {
        System.out.println("\nThis is a simple program which demonstrates examples of what I have learned\n" +
                "over the past semester.\n");
        System.out.println("Please select from the following options: ");
        System.out.println("Enter 1 for Collections Example");
        System.out.println("Enter 2 for Exception Example");
        System.out.println("Enter 3 for Thread Example");
        System.out.println("Enter 4 for JUnit Example");
        System.out.println("Enter 5 to Quit");

        Scanner scanIn = new Scanner(System.in);
        int scanEntry = 0;
        boolean valid = false;
        while (! valid) {
            System.out.println("Please enter a number from 1 to 5.");
            try {
                scanEntry = scanIn.nextInt();
                if (scanEntry >= 1 && scanEntry <= 5) {
                    valid = true;
                } else {
                    System.out.println("The number you entered is outside the range.");
                }
                } catch (InputMismatchException e) {
                    System.out.println("That was not a valid entry.");
                    scanIn.next();
                }
            }

        switch (scanEntry) {
            case 1 -> {
                CollectionApp Collect = new CollectionApp();
                Collect.Collect();
            }
            case 2 -> {
                ExceptionApp Except = new ExceptionApp();
                Except.Except();
            }
            case 3 -> {
                ThreadApp run = new ThreadApp();
                run.run();
                ThreadApp Thread = new ThreadApp();
                Thread.ThreadRun();
            }
            case 4 -> {
                JunitAPP JUnit = new JunitAPP();
                JUnit.JUnit();
            }
            case 5 -> {
                System.out.println("Thank you for using this simple app.");
                System.exit(0);
            }
        }
    }
}


