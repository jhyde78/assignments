
// Jarom Hyde

package FinalApp;

import java.util.InputMismatchException;
import java.util.Scanner;

class ThreadApp implements Runnable{

    public void run() {
        for(int i=0; i < 10; i++) {
            System.out.println("Thread number: " + i);
            try {
                Thread.sleep(10);
            }
            catch(InterruptedException ie){
                System.out.println("The thread has been interrupted. " + ie );
            }
        }

        System.out.println("The thread is complete.");
    }

    public void ThreadRun() {

        Thread t = new Thread(new ThreadApp(), "My Thread");

        t.start();

        for(int i=0; i < 10; i++) {

            System.out.println("Main Thread: " + i);
            try {
                Thread.sleep(20);
            }
            catch(InterruptedException ie) {
                System.out.println("The thread has been interrupted. " + ie);
            }
        }
        System.out.println("Main Thread complete.");
    }

    static {

        System.out.println("Thank you for trying my Thread App.\n");
        System.out.println("Now where would you like to go?\n");
        System.out.println("Please select from the following options: ");
        System.out.println("Enter 1 for Collections Example");
        System.out.println("Enter 2 for Exception Example");
        System.out.println("Enter 3 for JUnit Example");
        System.out.println("Enter 4 to quit");
        System.out.println("Please enter a number from 1 to 4.");

        Scanner scanIn = new Scanner(System.in);
        int scanEntry = 0;
        boolean valid = false;
        while (!valid) {
            try {
                scanEntry = scanIn.nextInt();
                if (scanEntry >= 1 && scanEntry <= 4) {
                    valid = true;
                } else {
                    System.out.println("The number you entered is outside the range.");
                }
            } catch (InputMismatchException ex) {
                System.out.println("That was not a valid entry.");
                scanIn.next();
            }
        }

        switch (scanEntry) {
            case 1 -> {
                CollectionApp Collect = new CollectionApp();
                Collect.Collect();
            }
            case 2 -> {
                ExceptionApp Except = new ExceptionApp();
                Except.Except();
            }
            case 3 -> {
                JunitAPP JUnit= new JunitAPP();
                JUnit.JUnit();
            }
            case 4 -> {
                System.out.println("Thank you for using this simple app.");
                System.exit(0);
            }
        }
    }
}

