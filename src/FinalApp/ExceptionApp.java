
// Jarom Hyde

package FinalApp;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.lang.*;

public class ExceptionApp {
    static void Except() {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("This portion of the app will handle data entry validation.");
        System.out.println("Here's a simple division program.");
        System.out.println("\nPlease enter a number: ");
        int numerator;
        numerator = keyboard.nextInt();
        System.out.println();
        System.out.println("Please enter another number that is not zero: ");
        int denominator;
        denominator = keyboard.nextInt();
        System.out.println();


        while (denominator == 0) {
            System.out.println("You entered zero. Please enter a number that is not zero: ");
            denominator = keyboard.nextInt();
            System.out.println();
        }

        try {
            int result = numerator / denominator;
            System.out.println(numerator + " divided by " + denominator + " is " + result + ".");
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        }
        System.out.println();
        System.out.println("If you are reading this, then the try block worked to completion.");
        System.out.println("Or the catch block caught the exception and the code continued to completion.");

        }

        static {
            System.out.println("Thank you for trying my Exception App.\n");
            System.out.println("Now where would you like to go?\n");
            System.out.println("Please select from the following options: ");
            System.out.println("Enter 1 for Collections Example");
            System.out.println("Enter 2 for Thread Example");
            System.out.println("Enter 3 for JUnit Example");
            System.out.println("Enter 4 to quit");
            System.out.println("Please enter a number from 1 to 4.");

        Scanner scanIn = new Scanner(System.in);
        int scanEntry = 0;
        boolean valid = false;
        while (!valid) {
            try {
                scanEntry = scanIn.nextInt();
                if (scanEntry >= 1 && scanEntry <= 4) {
                    valid = true;
                } else {
                    System.out.println("The number you entered is outside the range.");
                }
            } catch (InputMismatchException ex) {
                System.out.println("That was not a valid entry.");
                scanIn.next();
            }
        }

            switch (scanEntry) {
                case 1 -> {
                    CollectionApp Collect = new CollectionApp();
                    Collect.Collect();
                }
                case 2 -> {
                    ThreadApp run = new ThreadApp();
                    run.run();
                    ThreadApp Thread = new ThreadApp();
                    Thread.ThreadRun();
                }
                case 3 -> {
                    JunitAPP JUnit = new JunitAPP();
                    JUnit.JUnit();
                }
                case 4 -> {
                    System.out.println("Thank you for using this simple app.");
                    System.exit(0);
                }
            }
    }
}





