
// Jarom Hyde

package FinalApp;

import java.util.InputMismatchException;
import java.util.Scanner;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JunitAPP {

    @Test
    public void JUnit() {
        System.out.println("This app goes through JUnit assertions.");
        JUnitFormula test = new JUnitFormula();
        int result = test.math(4, 2, 4, 2);

        assertNotNull(result);

        assertTrue(5 <= result);

        assertFalse(5 >= result);

        assertSame(10, result);

        assertNotSame(5, result);

        assertEquals(10, result);

        System.out.println("\nIt appears the JUnit App worked.");

        System.out.println("Thank you for trying my JUnit App.\n");
        System.out.println("Now where would you like to go?\n");
        System.out.println("Please select from the following options: ");
        System.out.println("Enter 1 for Collections Example");
        System.out.println("Enter 2 for Exception Example");
        System.out.println("Enter 3 for Thread Example");
        System.out.println("Enter 4 to quit");
        System.out.println("Please enter a number from 1 to 4.");

        Scanner scanIn = new Scanner(System.in);
            int scanEntry = 0;
            boolean valid = false;
            while (!valid) {
                try {
                    scanEntry = scanIn.nextInt();
                    if (scanEntry >= 1 && scanEntry <= 4) {
                        valid = true;
                    } else {
                        System.out.println("The number you entered is outside the range.");
                    }
                } catch (InputMismatchException e) {
                    System.out.println("That was not a valid entry.");
                    scanIn.next();
                }
            }

        switch (scanEntry) {
            case 1 -> {
                CollectionApp Collect = new CollectionApp();
                Collect.Collect();
            }
            case 2 -> {
                ExceptionApp Except = new ExceptionApp();
                Except.Except();
            }
            case 3 -> {
                ThreadApp run = new ThreadApp();
                run.run();
                ThreadApp Thread = new ThreadApp();
                Thread.ThreadRun();
            }
            case 4 -> {
                System.out.println("Thank you for using this simple app.");
                System.exit(0);
            }
        }

    }
}
