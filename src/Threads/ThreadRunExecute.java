
// Jarom Hyde

package Threads;

public class ThreadRunExecute implements Runnable{

    public void run() {
        for(int i=0; i < 10; i++) {
            System.out.println("Thread number: " + i);
                try {
                    Thread.sleep(10);
                }
                catch(InterruptedException ie){
                    System.out.println("The thread has been interrupted. " + ie );
                }
        }

        System.out.println("The thread is complete.");
    }

    public static void main(String[] args) {

        Thread t = new Thread(new ThreadRunExecute(), "My Thread");

        t.start();

        for(int i=0; i < 10; i++) {

            System.out.println("Main Thread: " + i);
            try {
                Thread.sleep(20);
            }
            catch(InterruptedException ie) {
                System.out.println("The thread has been interrupted. " + ie);
            }
        }
        System.out.println("Main Thread complete.");
    }
}
