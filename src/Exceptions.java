
// Jarom Hyde

import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        // Get user input of two integers
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Here's a simple division program.");
        System.out.println("Please enter a number: ");
            int numerator;
            numerator = keyboard.nextInt();
        System.out.println();
        System.out.println("Please enter another number that is not zero: ");
            int denominator;
            denominator = keyboard.nextInt();
        System.out.println();

        // Comment out this while statement to turn off the validation of the denominator, try catch will still
        // work
        while (denominator == 0) {
            System.out.println("You entered zero. Please enter a number that is not zero: ");
            denominator = keyboard.nextInt();
            System.out.println();
        }

        // Comment out lines 23-27, 30, and 33-36 have the program crash without completion
            try {
                int result = numerator / denominator;
                System.out.println(numerator + " divided by " + denominator + " is " + result + ".");
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
                ex.printStackTrace();
            }
            System.out.println();
            System.out.println("If you are reading this, then the try block worked to completion.");
            System.out.println("Or the catch block caught the exception and the code continued to completion.");
    }
}
