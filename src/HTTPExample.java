
// Jarom Hyde

import java.net.*;
import java.io.*;
import java.util.*;

public class HTTPExample {
    public static String getHTTPContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHttpHeaders(String string) {

        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;

    }
    public static void main(String[] args) {
        System.out.println(HTTPExample.getHTTPContent("http://games-workshop.com"));

        Map<Integer, String> m = HTTPExample.getHttpHeaders("http://games-workshop.com");

        for (Map.Entry<Integer,String> entry : m.entrySet()) {
            System.out.println("Key = " + entry.getKey());
        }
    }
}
