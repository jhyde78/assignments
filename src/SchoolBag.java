
import java.util.ArrayList;

// Jarom Hyde

public class SchoolBag {

    public static void main(String[] args) {

        // Declare the arraylist of itemsSchoolBag
            ArrayList<String> itemsSchoolBag = new ArrayList<>();

        // Declare the items in the school bag
            itemsSchoolBag.add("MacBook Air");
            itemsSchoolBag.add("Air Pods");
            itemsSchoolBag.add("Drawing Tablet");
            itemsSchoolBag.add("Red Pen");
            itemsSchoolBag.add("4GB Thumb Drive");
            itemsSchoolBag.add("Power Cable");

        // Print items in school bag

            System.out.println("Current items:");
            System.out.println();
            for (int i = 0; i < itemsSchoolBag.size(); i++) {
                System.out.println(itemsSchoolBag.get(i));
        }
        // Print out line break
            System.out.println();

        // Removing the red pen
            itemsSchoolBag.remove(itemsSchoolBag.size() - 3);
            System.out.println();
            System.out.println("You have removed the red pen from the school bag.");
        // Removing the 4GB thumb drive
            itemsSchoolBag.remove(itemsSchoolBag.size() - 2);
            System.out.println();
            System.out.println("You have removed the 4GB thumb drive from the bag.");

        // Adding a blue pen
            itemsSchoolBag.add("Blue Pen");
            System.out.println();
            System.out.println("You have added a blue pen to the school bag.");
        // Adding a 64GB thumb drive
            itemsSchoolBag.add("64GB Thumb Drive");
            System.out.println();
            System.out.println("You have added a 64GB thumb drive to the bag.");
            System.out.println();

        // Print current contents of the school bag
            for (String value: itemsSchoolBag) {
                System.out.println(value);

        }
            System.out.println();
            System.out.println("You are ready for school!");
    }
}
