package JSON;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONExample {

        public static String primarisToJSON(Primaris primaris) {

            ObjectMapper mapper = new ObjectMapper();
            String s = "";

        try {
            s = mapper.writeValueAsString(primaris);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
            return s;
        }

        public static Primaris JSONToPrimaris(String s) {

            ObjectMapper mapper = new ObjectMapper();
            Primaris primaris = null;

            try {
                primaris = mapper.readValue(s, Primaris.class);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return primaris;
        }

    public static void main(String[] args) {

            Primaris prim = new Primaris();
            prim.setName("Primaris Astartes");
            prim.setUnit("Lieutenant");

            String json = JSONExample.primarisToJSON(prim);
            System.out.println(json);

            Primaris prim2 = JSONExample.JSONToPrimaris(json);
            System.out.println(prim2);

    }
}

